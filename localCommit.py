#!/usr/bin/env python
#coding=utf-8

import argparse
import db

def getArgs():
    parser = argparse.ArgumentParser()
    parser.add_argument('--file', '-f', help = 'json file')
    return parser.parse_args()

def importProcess(filePath):
    if filePath:
        db.saveProcess(open(filePath).read())

if __name__ == '__main__':
    importProcess(getArgs().file)
