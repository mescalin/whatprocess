#!/usr/bin/env python
#coding=utf-8

import hashlib
import json
import sys
import urlparse
import web
from web.contrib.template import render_jinja

import db
import log

render = render_jinja('templates', encoding='utf-8')

class Nothing:
    def GET(self):
        return '\n'.join([repr(e) for e in web.ctx.iteritems()])

class Index:
    def GET(self, name):
        return render.index()

class Search:
    def GET(self, name):
        return json.dumps(db.searchProces(name))

class Commit:
    def GET(self):
        return render.commit()
    def POST(self):
        if db.commitProess(json.loads(web.data())):
            return web.ok()
        return web.notfound()

class Confirm:
    def GET(self):
        query = urlparse.parse_qs(web.ctx.env['QUERY_STRING'])
        key = query['key'][0]
        method = query['method'][0]
        if key != 'jxyIlydR': #验证key
            return web.notfound()
        if method == 'get': #获取列表
            commits = db.getCommit()
            l = [{k.decode('utf8'): v.decode('utf8') for k, v in x.items()} for x in commits]
            return render.confirm(commits=l)
        cid = query['id'][0]
        if method == 'del': #删除一个commit
            db.delCommit(cid)
            return web.ok()
        if method == 'confirm': #确认一个 commit
            db.confirm(cid)
            return web.ok()
        return web.notfound()

class Cross:
    def GET(self):
        web.header('Content-Type', 'application/xml')
        return '''<cross-domain-policy>
<allow-access-from domain="*" to-ports="*"/>
<allow-access-from domain="localhost" to-ports="*"/>
</cross-domain-policy>'''

_URLS = (
    '/whatprocess/search/(.+)', Search,
    '/whatprocess/commit', Commit,
    '/whatprocess/confirm', Confirm,
    '/crossdomain.xml', Cross,
    '/whatprocess/test', Nothing,
    '/(.*)', Index,
)

application = web.application(_URLS, globals()).wsgifunc()
