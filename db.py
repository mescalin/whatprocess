#!/usr/bin/env python
#coding=utf-8

import json
import time
import redis

import log

_REDIS_TIMEOUT = 10 #10 s

_ID = 'id'
_NAME = 'name'
_LIKE = 'like'
_AUTHORS = 'authors'
_DESCRIPTION = 'des'
_CREATE_TIME = 'create_time'

_PROCESSID = '_PROCESSID'
_COMMITID = '_COMMITID'
_ZSCOMMIT = 'zs:commit' #提交后的保存在这里

_HOST = 'localhost:6379'
_CLIENT = None
def _getClient():
    global _CLIENT
    if _CLIENT:
        return _CLIENT
    try:
        host, port = _HOST.split(':')
        _CLIENT = redis.StrictRedis(host, int(port), socket_timeout = _REDIS_TIMEOUT, db = 0)
        return _CLIENT
    except Exception as e:
        log.logger.error(e.message)
        return None

def _getKeyProcess(pid): #p:<process_id>
    return 'p:' + str(pid)

def _getKeyCommit(cid): #c:<commit_id>
    return 'c:' + str(cid)

def _getKeyProcessName(name): #pn:<name>
    return 'pn:' + name


#proc is a dict
#在这个函数内判断 proc 的参数合法性
def _saveProcess(proc):
    try:
        if not proc.has_key(_NAME) or \
           not proc.has_key(_DESCRIPTION) or \
           len(proc[_NAME]) > 128 or \
           len(proc[_DESCRIPTION]) > 4096:
            log.logger.error('bad format:{}'.format(proc))
            return False
        proc[_NAME] = proc[_NAME].strip().lower() #process name lower
        proc[_DESCRIPTION] = proc[_DESCRIPTION].strip()
        proc[_CREATE_TIME] = int(time.time())
        name = _getKeyProcessName(proc[_NAME])
        client = _getClient()
        if client.exists(name):
            log.logger.error('key exists:{}, dict:{}'.format(name, proc))
            return False
        pid = client.incr(_PROCESSID)
        pl = client.pipeline(transaction=False)
        pl.set(name, pid) #pn:<name> -> pid
        pl.hmset(_getKeyProcess(pid), proc) #p:<pid> -> {name:'', des:''}
        pl.execute()
    except Exception as e:
        log.logger.error(e.message)
        return False
    return True

_LAST_COMMIT = 0
def commitProess(proc):
    global _LAST_COMMIT
    now = time.time()
    delta = now - _LAST_COMMIT
    if delta < 5:
        log.logger.error('commit too frequently, delta:{}, proc:{}'.format(delta, proc))
        return False
    try:
        if not proc.has_key(_NAME) or \
           not proc.has_key(_DESCRIPTION) or \
           len(proc[_NAME]) > 128 or \
           len(proc[_DESCRIPTION]) > 4096:
            log.logger.error('bad format:{}'.format(proc))
            return
        proc[_NAME] = proc[_NAME].strip().lower() #process name lower
        proc[_DESCRIPTION] = proc[_DESCRIPTION].strip()
        proc[_CREATE_TIME] = int(time.time())
        client = _getClient()
        cid = client.incr(_COMMITID)
        pl = client.pipeline(transaction=False)
        pl.hmset(_getKeyCommit(cid), proc) #c:<cid> -> {name:'', des:''}
        pl.zadd(_ZSCOMMIT, proc[_CREATE_TIME], cid)
        pl.execute()
        _LAST_COMMIT = now
        return True
    except Exception as e:
        log.logger.error(e.message)
        return False

def confirm(cid):
    client = _getClient()
    proc = client.hgetall(_getKeyCommit(cid))
    if not proc:
        log.logger.error('can not find:{}'.format(_getKeyCommit(cid)))
        return False
    if client.exists(_getKeyProcessName(proc[_NAME])):
        log.logger.error('{} exists'.format(proc[_NAME]))
        return False
    if _saveProcess(proc):
        delCommit(cid)
        return True
    return False

def delCommit(cid):
    pl = _getClient().pipeline(transaction=False)
    pl.delete(_getKeyCommit(cid))
    pl.zrem(_ZSCOMMIT, cid)
    pl.execute()

def saveProcess(proc): #proc is json
    processes = json.loads(proc)
    if type(processes) == type([]):
        for elt in processes:
            _saveProcess(elt)
    elif type(processes) == type({}):
        _saveProcess(processes)

def searchProces(name):
    name = name.strip().lower()
    client = _getClient()
    pid = client.get(_getKeyProcessName(name))
    if not pid:
        return {}
    return client.hgetall(_getKeyProcess(pid))

def getCommit(): #获取已经提交的，按时间大小排序
    l = []
    client = _getClient()
    for elt in client.zrange(_ZSCOMMIT, 0, 30):
        proc = client.hgetall(_getKeyCommit(elt))
        if not proc:
            continue
        proc[_ID] = elt
        proc[_CREATE_TIME] = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(int(proc[_CREATE_TIME])))
        l.append(proc)
    return l
